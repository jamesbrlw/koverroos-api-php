<?php
// Routes
#######################
#configure big commerce
#######################
use Bigcommerce\Api\Client as Bigcommerce;

$api_key = getenv('BIG_COMMERCE_API_KEY') ;
$username = getenv('BIG_COMMERCE_USERNAME');
$store_url = getenv('BIG_COMMERCE_STORE_URL'); 
// $bigcommerce = Bigcommerce;

Bigcommerce::configure(array(
    'store_url' => $store_url,
    'username'  => $username,
    'api_key'   => $api_key
));

$app->get('/cache_flush', function ($request, $response, $args) use ($memcache){
  $memcache->flush();
  return $response->write( 'splsssssssssssssshhhhhhh gurgle gurgle gurgle');
  });

$app->get('/products/{ids}', function ($request, $response, $args) use ($memcache){
  
  $product_ids = explode(",", $args['ids']);
  $products = array();
  $product_fields = array('id','name','price','description');
  $image_fields = array('id','image_file','zoom_url','thumbnail_url','standard_url','tiny_url','is_thumbnail','sort_order');
  $custom_fields_fields = array('id','name','text');

  foreach ($product_ids as $id) {
    $mem_key = "product_" . $id;
    if($p = $memcache->get($mem_key)) {
      //do nothing
    } else {
      
      $product = Bigcommerce::getProduct($id);
      if($product === false) {
        continue;
      }

      $images = array();

      $p = array();
      foreach($product_fields as $key){
        $p[$key] = $product->{$key};
      }

      $p['images'] = array_map(function($image) use ($image_fields){ 
        return array_reduce($image_fields, function($v1, $v2) use ($image){
          $v1[$v2] = $image->{$v2};
          return $v1;
        }, array());

      }, $product->images);

      $p['custom_fields'] = array_map(function($fields) use ($custom_fields_fields){ 
        return array_reduce($custom_fields_fields, function($v1, $v2) use ($fields){
          $v1[$v2] = $fields->{$v2};
          return $v1;
        }, array());

      }, (array)$product->custom_fields);

      $memcache->set($mem_key, $p, MEMCACHED_EXPIRE);
    }

      
    $products[] = $p;
  }

  $newResponse = $response->withHeader('Content-type', 'application/json');
  return $newResponse->write( json_encode($products, JSON_PRETTY_PRINT));
});
