<?php
// Application middleware
use \CorsSlim\CorsSlim as CorsSlim;
// e.g: $app->add(new \Slim\Csrf\Guard);
$corsOptions = array(
    "origin" => "*",
    "exposeHeaders" => array("Content-Type", "X-Requested-With", "X-authentication", "X-client"),
    "allowMethods" => array('GET', 'POST', 'PUT', 'DELETE', 'OPTIONS')
);
$cors = new CorsSlim($corsOptions);
$app->add($cors);